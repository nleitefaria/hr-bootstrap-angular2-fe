import { Injectable } from '@angular/core';
import { ICountries } from './countries';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class CountriesService 
{
  constructor(private http: Http) { }
  
  getCountries() 
  {
		return this.http.get(`http://localhost:8081/hr-spring-oracle-be/countries`)
						.map((res:Response) => <ICountries[]>res.json());
  }
  
  getCountriesById = (id: string): Observable<ICountries> => 
  {
		return this.http.get(`http://localhost:8081/hr-spring-oracle-be/countries/` + id)
						//.map((res:Response) => <ICountries>res.json());
						.map(response => response.json());												
  }
}
