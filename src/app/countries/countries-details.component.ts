import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CountriesService } from './countries.service';
import { ICountries } from './countries';

@Component({
  selector: 'app-countries-details',
  templateUrl: './countries-details.component.html',
  styleUrls: ['./countries-details.component.scss']
})
export class CountriesDetailsComponent implements OnInit {

  id: string;
  private sub: any;
  countries : ICountries; 
  cid: number;
  desc: string;
  
  constructor(private route: ActivatedRoute, private countriesService: CountriesService) 
  {
  }

  ngOnInit() 
  {
    this.sub = this.route.params.subscribe(params => {
    this.id = params['id']; // (+) converts string 'id' to a number
	this.desc = "Showing details for country: " + this.id;

    // In a real app: dispatch action to load the details here.
	   
	this.countriesService.getCountriesById(this.id).subscribe((data: ICountries) => this.countries = data,
                error => console.log(error),
                () => console.log('Get all Items complete' + this.countries.countryId + this.countries.countryName));   
    });
  }
}