import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CountriesComponent } from './countries/countries.component';
import { CountriesDetailsComponent } from './countries/countries-details.component';
import { RegionsComponent } from './regions/regions.component';

const routes: Routes = [
  {
    path: '',
    children: []
  },
  { path: 'countries', component: CountriesComponent },
  { path: 'countries-details/:id', component: CountriesDetailsComponent },
  { path: 'regions', component: RegionsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
