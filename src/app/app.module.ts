import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

//COMPONENTS
import { AppComponent } from './app.component';
import { CountriesComponent } from './countries/countries.component';
import { CountriesDetailsComponent } from './countries/countries-details.component';
import { RegionsComponent } from './regions/regions.component';

//SERVICES
import { CountriesService } from './countries/countries.service';




@NgModule({
  declarations: [
    AppComponent,
    CountriesComponent,
    CountriesDetailsComponent,
    RegionsComponent
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [CountriesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
