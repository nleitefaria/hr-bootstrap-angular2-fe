import { HrBootstrapAngular2FePage } from './app.po';

describe('hr-bootstrap-angular2-fe App', () => {
  let page: HrBootstrapAngular2FePage;

  beforeEach(() => {
    page = new HrBootstrapAngular2FePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
